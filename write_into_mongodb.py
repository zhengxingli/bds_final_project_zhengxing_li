import pymongo
from pymongo import MongoClient
import pandas as pd

mydata = pd.read_csv(
    "/Users/lizhengxing/Desktop/BDS/BDS_Final_Project_Zhengxing_Li/FinalProjectData1718.csv", header=None,
    names=['studentid', 'Lab_1', 'Christmas_Test', 'Lab_2', 'Easter_Test', 'Lab_3', 'part_time_job', 'Exam_Grade'], delimiter=';', skiprows=1)

studentid = mydata['studentid'].values
Lab_1 = mydata['Lab_1'].values
Christmas_Test = mydata['Christmas_Test'].values
Lab_2 = mydata['Lab_2'].values
Easter_Test = mydata['Easter_Test'].values
Lab_3 = mydata['Lab_3'].values
part_time_job = mydata['part_time_job'].values
Exam_Grade = mydata['Exam_Grade'].values


client = MongoClient('localhost', 27017)
db = client.test
collection = db.records
for i in range(len(studentid)):
    insert = db.records.insert_one({
        "studentid" : int(studentid[i]),
        "Lab_1" : int(Lab_1[i]),
        "Christmas_Test" : int(Christmas_Test[i]),
        "Lab_2" : int(Lab_2[i]),
        "Easter_Test" : int(Easter_Test[i]),
        "Lab_3" : int(Lab_3[i]),
        "part_time_job" : int(part_time_job[i]),
        "Exam_Grade": int(Exam_Grade[i])
    })
