// Sharding
mkdir ./shard1 ./shard2 ./shard3
mongod --shardsvr --replSet shard27021 -dbpath ./shard1 -port 27021
mongod --shardsvr --replSet shard27022 -dbpath ./shard2 -port 27022
mongod --shardsvr --replSet shard27023 -dbpath ./shard3 -port 27023
mongo localhost:27021
mongo localhost:27022
mongo localhost:27023

rs.initiate(
  {
    _id: "shard27021",
    members: [
      { _id : 0, host : "localhost:27021" },
    ]
  }
);

rs.initiate(
  {
    _id: "shard27022",
    members: [
      { _id : 0, host : "localhost:27022" },
    ]
  }
);

rs.initiate(
  {
    _id: "shard27023",
    members: [
      { _id : 0, host : "localhost:27023" },
    ]
  }
);

mkdir ./shardConfig
mongod --configsvr --replSet shardRepl --dbpath ./shardConfig --port 27024
rs.initiate(
  {
    _id: "shardRepl",
    configsvr: true,
    members: [
      { _id : 0, host : "localhost:27024" },
    ]
  }
)


mongos --configdb shardRepl/localhost:27024 --port 27025

mongo localhost:27025/admin

sh.addShard( "shard27021/localhost:27021");
sh.addShard( "shard27022/localhost:27022");
sh.addShard( "shard27023/localhost:27023");

db.adminCommand({enablesharding:"test"});
db.adminCommand({shardcollection:"test.records",key:{studentid:1}});

db.records.getShardDistribution();
