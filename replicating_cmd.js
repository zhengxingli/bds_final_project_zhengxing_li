//Replicating
mkdir ./repl1 ./repl2 ./repl3
mongod --replSet replTest -dbpath ./repl1 -port 27026 -rest
mongod --replSet replTest -dbpath ./repl2 -port 27027 -rest
mongod --replSet replTest -dbpath ./repl3 -port 27028 -rest

mongo localhost:27026
mongo localhost:27027
mongo localhost:27028

rs.initiate({
  _id:'replTest',
  members:[
    {_id:1,host:'localhost:27026'},
    {_id:2,host:'localhost:27027'},
    {_id:3,host:'localhost:27028'}
  ]
});

rs.status();

rs.slaveOk(true);
