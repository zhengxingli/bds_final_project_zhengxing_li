import pandas as pd
import psycopg2

mydata = pd.read_csv(
    "/Users/lizhengxing/Desktop/BDS/BDS_Final_Project_Zhengxing_Li/FinalProjectData1718.csv", header=None,
    names=['studentid', 'Lab_1', 'Christmas_Test', 'Lab_2', 'Easter_Test', 'Lab_3', 'part_time_job', 'Exam_Grade'], delimiter=';', skiprows=1)

studentid = mydata['studentid'].values
Lab_1 = mydata['Lab_1'].values
Christmas_Test = mydata['Christmas_Test'].values
Lab_2 = mydata['Lab_2'].values
Easter_Test = mydata['Easter_Test'].values
Lab_3 = mydata['Lab_3'].values
part_time_job = mydata['part_time_job'].values
Exam_Grade = mydata['Exam_Grade'].values

data = [[int(studentid[i]), int(Lab_1[i]),
         int(Christmas_Test[i]),  int(Lab_2[i]),  int(Easter_Test[i]), int(Lab_3[i]),  int(part_time_job[i]), int(Exam_Grade[i])] for i in range(len(studentid))]
# conn = None
# try:
#     conn = psycopg2.connect(
#         "dbname = 'records' user='' host='localhost' password = ''")
#     print("Connection success")
#
#     cur = conn.cursor()
#     cur.execute(
#     '''CREATE TABLE IF NOT EXISTS grades(
#         studentid SERIAL PRIMARY KEY,
#         Lab_1 int NOT NULL,
#         Christmas_Test int NOT NULL,
#         Lab_2 int NOT NULL,
#         Easter_Test int NOT NULL,
#         Lab_3 int NOT NULL,
#         part_time_job int NOT NULL,
#         Exam_Grade int NOT NULL)''')
#     cur.close()
#     conn.commit()
# except (Exception, psycopg2.DatabaseError) as error:
#     print(error)
# finally:
#     if conn is not None:
#         conn.close()

conn = None
try:
    conn = psycopg2.connect(
        "dbname = 'records' user='' host='localhost' password = ''")
    print("Connection success")
    cur = conn.cursor()
    # query = 'INSERT INTO grades(studentid, Lab_1, Christmas_Test, Lab_2, Easter_Test, Lab_3, part_time_job, Exam_Grade) VALUES ('+','.join(['%s']*len(data[0]))+');'
    for d in data:
        query = 'INSERT INTO grades(studentid, Lab_1, Christmas_Test, Lab_2, Easter_Test, Lab_3, part_time_job, Exam_Grade) VALUES ('+str(d[0])+','+str(d[1])+','+str(d[2])+','+str(d[3])+','+str(d[4])+','+str(d[5])+','+str(d[6])+','+str(d[7])+');'
        cur.execute(query)
    cur.close()
    conn.commit()
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()

#
#
# conn = None
# try:
#     conn = psycopg2.connect(
#         "dbname = 'movierecommendation' user='' host='localhost' password = ''")
#     print("Connection success")
#     cur = conn.cursor()
#     cur.execute("select * from movies")
#     rows = cur.fetchall()
#     print("\n Show me the data: \n")
#     for row in rows:
#         print(row, row[0], row[1])
# except:
#     print("I am unable to connect to the database")
# finally:
#     if conn is not None:
#         conn.close()
